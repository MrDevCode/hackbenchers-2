#!/usr/bin/python3

# Importing Libraries
from flask import Flask, render_template
import matplotlib.pyplot as pyplot
import csv
import math

# Defining Constants
CSV_FILENAME = "oec.csv"
SIGMA = 5.6704e-8
MIN_SURFACE_TEMP = 258.15 # Kelvin
MAX_SURFACE_TEMP = 395.15 #Kelvin

fields = []
rows = []

app = Flask(__name__)

# Home Page
@app.route('/')
def index():
	return render_template('index.html',
							planets_in_habitable_zone = habitable_zone(),
							planets_with_suitable_surface_temperature = surface_temperature())

# Function to check if the Planet is in the Habitable Zone
def habitable_zone ():
	planets_in_habitable_zone = []

	with open(CSV_FILENAME, 'r') as csvfile:
		csvreader = csv.reader(csvfile)
		next(csvreader, None)

		for row in csvreader:

			if (row[5]!='') and (row[6]!='') and (row[20]!='') and (row[22]!=''):
				host_star_temp = float(row[22])
				host_star_radius = float(row[20]) * 695508 * 1000 # Convert to m
				eccentricity = float(row[6])
				semi_major_axis = float(row[5]) * (1.496e+11) # Convert AU to km

				# Formula
				semi_minor_axis = ((semi_major_axis**2) * (1 - (eccentricity**2)))**(0.5)
				luminosity = 4 * math.pi * (host_star_radius ** 2) * SIGMA * (host_star_temp ** 4)
				luminosity /= 3.826e+26
				inner_radius = ((luminosity / 1.1) ** (0.5)) * (1.496e+11)
				outer_radius = ((luminosity / 0.53) ** (0.5)) * (1.496e+11)

				if (semi_major_axis <= outer_radius) and (semi_minor_axis >= inner_radius):
					planets_in_habitable_zone.append(row[0])

	return planets_in_habitable_zone

# Function to check if the Surface Temperature of an Planet is suitable
def surface_temperature ():
	planets_with_suitable_surface_temperature = []

	with open(CSV_FILENAME, 'r') as csvfile:
		csvreader = csv.reader(csvfile)
		next(csvreader, None)

		for row in csvreader:

			if row[11] != '':
				exoplanet_surface_temp = float(row[11])

				if (exoplanet_surface_temp >= MIN_SURFACE_TEMP) and (exoplanet_surface_temp <= MAX_SURFACE_TEMP):
					planets_with_suitable_surface_temperature.append(row[0])

	return planets_with_suitable_surface_temperature

def generate_charts ():
	c1 = c2 = c3 = 0
	count = []
	'''
	c1 -> count of Planets which satisfy the conditions
	c2 -> count of Planets which satisfy the conditions
	c3 -> count of Planets with insufficient data
	count = [c1, c2, c3]
	'''
	planet_labels = ['Planets which satisfy the conditions',
					 'Planets which do not satisfy the conditions',
					 'Planets with insufficient data']

	with open(CSV_FILENAME, 'r') as csvfile:
		csvreader = csv.reader(csvfile)
		next(csvreader, None)

		for row in csvreader:

			if row[11] != '':
				exoplanet_surface_temp = float(row[11])

				if (exoplanet_surface_temp >= MIN_SURFACE_TEMP) and (exoplanet_surface_temp <= MAX_SURFACE_TEMP):
					c1 += 1
				else:
					c2 += 1

			else:
				c3 += 1


	count = [c1, c2, c3]
	pyplot.pie(count, labels=planet_labels, startangle=90, autopct='%.1f%%')
	pyplot.savefig('static/surface_temperature.png', bbox_inches='tight')
	pyplot.clf()

	c1 = c2 = c3 = 0

	with open(CSV_FILENAME, 'r') as csvfile:
		csvreader = csv.reader(csvfile)
		next(csvreader, None)

		for row in csvreader:

			if (row[5]!='') and (row[6]!='') and (row[20]!='') and (row[22]!=''):
				host_star_temp = float(row[22])
				host_star_radius = float(row[20]) * 695508 * 1000 # Convert to m
				eccentricity = float(row[6])
				semi_major_axis = float(row[5]) * (1.496e+11) # Convert AU to km

				# Formula
				semi_minor_axis = ((semi_major_axis**2) * (1 - (eccentricity**2)))**(0.5)
				luminosity = 4 * math.pi * (host_star_radius ** 2) * SIGMA * (host_star_temp ** 4)
				luminosity /= 3.826e+26
				inner_radius = ((luminosity / 1.1) ** (0.5)) * (1.496e+11)
				outer_radius = ((luminosity / 0.53) ** (0.5)) * (1.496e+11)

				if (semi_major_axis <= outer_radius) and (semi_minor_axis >= inner_radius):
					c1 += 1
				else:
					c2 += 1

			else:
				c3 += 1

	count = [c1, c2, c3]
	pyplot.pie(count, labels=planet_labels, startangle=90, autopct='%.1f%%')
	pyplot.savefig('static/habitable_zone.png', bbox_inches='tight')


if __name__ == '__main__':
	generate_charts()
	app.run(host='0.0.0.0')